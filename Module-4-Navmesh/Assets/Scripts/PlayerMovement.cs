﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController controller;
    public float playerSpeed = 5;

    void Update() {
        movement();
    }

    void movement(){
    float Xtranslation = Input.GetAxis("Horizontal");
    float Ztranslation = Input.GetAxis("Vertical");

    float Ytranslation = 0;
    if (Input.GetKey(KeyCode.Space))
        Ytranslation += 0.5f;
    else if (Input.GetKey(KeyCode.LeftShift))
        Ytranslation -= 0.5f;

        Vector3 move = transform.right * Xtranslation + transform.forward * Ztranslation + transform.up * Ytranslation;

        controller.Move(move * playerSpeed * Time.deltaTime);
    }
}
