﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentManager : MonoBehaviour
{
    GameObject[] agents;
    [SerializeField] Vector3 offset;
    public Transform Player;
    void Start() {
        agents = GameObject.FindGameObjectsWithTag("AI");
    }

    void Update(){
        foreach (GameObject ai in agents) {
            ai.GetComponent<AIControl>().agent.SetDestination(Player.position - offset);
            }
    }
}
