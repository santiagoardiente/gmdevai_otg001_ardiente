﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankButtonHandler : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject[] TankButtons;


    public void activateGreen(){
        TankButtons[0].SetActive(true);
        TankButtons[1].SetActive(false);
        TankButtons[2].SetActive(false);
    }
    public void activateBlue(){
        TankButtons[0].SetActive(false);
        TankButtons[1].SetActive(true);
        TankButtons[2].SetActive(false);
    }
    public void activateRed(){
        TankButtons[0].SetActive(false);
        TankButtons[1].SetActive(false);
        TankButtons[2].SetActive(true);
    }
}
