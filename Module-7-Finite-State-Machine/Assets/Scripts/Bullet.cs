﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	public GameObject explosion;
	public float bulletDamage;
	
	void OnCollisionEnter(Collision col)
    {
    	GameObject e = Instantiate(explosion, this.transform.position, Quaternion.identity);
		if (col.gameObject.GetComponent<Health>())
			col.gameObject.GetComponent<Health>().takeDamage(bulletDamage);
    	Destroy(e,1.5f);
    	Destroy(this.gameObject);
    }
}
