﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public int playerSpeed = 5;
    public Transform head;


    void Update() {
        movement();
        faceMouse();
    }

    void movement(){
    float Xtranslation = Input.GetAxis("Horizontal") * playerSpeed * Time.deltaTime;
        float Ztranslation = Input.GetAxis("Vertical") * playerSpeed * Time.deltaTime;
        float Ytranslation = 0;

    if (Input.GetKey(KeyCode.Space))
        Ytranslation += (float)0.05;
    else if (Input.GetKey(KeyCode.LeftShift))
        Ytranslation -= (float)0.05;

        transform.position += new Vector3(Xtranslation, Ytranslation, Ztranslation);
    }

    void faceMouse() {
        Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        float midPoint = (transform.position - Camera.main.transform.position).magnitude * 0.5f;
        head.transform.LookAt(mouseRay.origin + mouseRay.direction * midPoint);
    }
}
