﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TravelForwardToGoal : MonoBehaviour
{
    public Transform goal;
    public float speed = 5;
    public float rotSpeed = 4;
    void Start(){
    }
    void LateUpdate() {
        Vector3 lookAtGoal = new Vector3(goal.position.x, transform.position.y, goal.position.z);
        Vector3 direction = lookAtGoal - transform.position;
        
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction),Time.deltaTime * rotSpeed);

        if(Vector3.Distance(goal.position, transform.position) > 1) {
            transform.position =  Vector3.Lerp(transform.position, lookAtGoal, speed * Time.deltaTime);
        }
    }
}